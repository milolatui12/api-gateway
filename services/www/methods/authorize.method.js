const _ = require('lodash');

module.exports = async function (ctx, route, req) {
	ctx.meta.clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
	ctx.meta.auth = ctx.meta.user.user
	ctx.meta.deviceId = ctx.meta.user.deviceId
	delete ctx.meta.user;
};
//ctx.meta.auth = ctx.meta.user;